import sys from time import sleep
from awsUtils import upload_to_s3, create_new_version, deploy_new_version

#Environments
ENV_NAME = "APPLICATION_ENVIRONMENT_DEV"
ENV_NAME = "APPLICATION_ENVIRONMENT_DEV_STABLE"
ENV_NAME = "APPLICATION_ENVIRONMENT_STAGING"
ENV_NAME = "APPLICATION_ENVIRONMENT"



def main():
    " Your favorite wrapper's favorite wrapper "
    if not upload_to_s3('/tmp/artifact.zip'):
        sys.exit(1)
    sleep(3)
    if not create_new_version():
        sys.exit(1)
    # Wait for the new version to be consistent before deploying
    sleep(7)
    if not deploy_new_version(environmentName=ENV_NAME):
        sys.exit(1)

if __name__ == "__main__":
    main()
