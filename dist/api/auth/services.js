"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _jsonwebtoken = _interopRequireDefault(require("jsonwebtoken"));

var _lodash = _interopRequireDefault(require("lodash"));

var _axios = _interopRequireDefault(require("axios"));

var _string = _interopRequireDefault(require("string"));

var _async = _interopRequireDefault(require("async"));

var _constants = _interopRequireDefault(require("services/constants"));

var _errors = require("services/errors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Third party modules
// In house modules
const {
  AWS_SECRET,
  ROUTES: {
    USER
  }
} = _constants.default;
const AuthServices = {
  issueToken: (payload, time) => _jsonwebtoken.default.sign(payload, AWS_SECRET, {
    expiresIn: time
  }),
  verifyToken: token => new Promise((resolve, reject) => {
    _jsonwebtoken.default.verify(token, AWS_SECRET, (err, decoded) => {
      if (err) reject(err);
      resolve({
        valid: true,
        content: decoded
      });
    });
  }),
  checkRoute: (routes, path, method) => new Promise(resolve => {
    const matched = _lodash.default.find(routes[method], route => (0, _string.default)(path).include(route));

    const exist = _lodash.default.includes(routes[method], matched);

    resolve(exist);
  }),
  getParts: authorization => {
    const parts = authorization.split(' ');
    return {
      scheme: parts[0],
      token: parts[1],
      size: parts.length
    };
  },
  validFormatToken: parts => parts.size === 2 && /^Bearer$/i.test(parts.scheme),

  authErrMsg(msg) {
    const errMsg = msg ? msg : 'Require Authorization';
    return (0, _errors.unAuthorized)(errMsg);
  },

  checkAuthorization: authorization => new Promise((resolve, reject) => {
    const parts = AuthServices.getParts(authorization);
    const validFormat = AuthServices.validFormatToken(parts);
    if (!validFormat) reject();
    AuthServices.verifyToken(parts.token).then(({
      content
    }) => resolve(content)).catch(err => reject(err));
  }),

  async authMidleware(req, res, next) {
    const {
      path,
      headers: {
        authorization
      },
      method,
      query
    } = req;
    const {
      restricted
    } = query;

    try {
      const isPublic = await AuthServices.checkRoute(USER, path, method);

      if (restricted && method === 'GET') {
        _lodash.default.set(req, 'isRestricted', true);

        next();
      } else if (isPublic && !restricted) {
        if (authorization) {
          await AuthServices.checkAuthorization(authorization).then(tokenContent => {
            _lodash.default.set(req, 'user', tokenContent);
          }).catch(() => {});
        }

        next();
      } else if (authorization) {
        AuthServices.checkAuthorization(authorization).then(tokenContent => {
          _lodash.default.set(req, 'user', tokenContent);

          next();
        }).catch(() => {
          res.unauthorized(AuthServices.authErrMsg('Format is Authorization: Bearer [token]'));
        });
      } else {
        throw AuthServices.authErrMsg();
      }
    } catch (err) {
      res.unauthorized(AuthServices.authErrMsg(err.message));
    }
  },

  facebook: (accessToken, userId) => new Promise((resolve, reject) => {
    // Request to FB
    const API = `https://graph.facebook.com/${userId}?access_token=${accessToken}&fields=id,first_name,last_name,birthday,gender,verified,email`;

    _axios.default.get(API).then(({
      data
    }) => resolve(data)).catch(error => reject(error));
  }),
  likesPaging: next => new Promise((resolve, reject) => {
    _axios.default.get(next).then(({
      data
    }) => JSON.parse(data)).catch(error => reject(error));
  }),
  getFacebookLikes: accessToken => new Promise((resolve, reject) => {
    const limit = 100;
    const API = `https://graph.facebook.com/me/likes?limit=${limit}&access_token=${accessToken}`;
    const likesArray = [];

    _axios.default.get(API).then(({
      data
    }) => {
      let url = data.paging.next;
      likesArray.push(data.data);
      if (typeof url === 'undefined') resolve(likesArray);

      _async.default.whilst(() => !_lodash.default.isNull(url), callback => {
        AuthServices.likesPaging(url).then(likes => {
          likesArray.push(likes.data);
          url = likes.data.length < limit ? null : likes.paging.next;
          callback(null, likesArray);
        });
      }, (error, likesPlayer) => {
        if (error) reject(error);
        resolve(likesPlayer);
      });
    }).catch(({
      response
    }) => reject(response));
  })
};
var _default = AuthServices;
exports.default = _default;
module.exports = exports.default;