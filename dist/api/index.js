"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _services = require("./auth/services.js");

const router = new _express.Router();
/**
 * @apiDefine authorizationHeaders
 * @apiHeader (Headers) {String} Authorization JSON Web Token (Facebook Access Token if the endpoint is /auth)
 * @apiHeaderExample {json} Example
 *  {
 *    "Authorization": "Bearer JWT_TOKEN"
 *  }
 *
 * @apiErrorExample {json} Authorization Error
 HTTP/1.1 401 Unauthorized
 {
    "errors": {
      "code": 401,
      "content": {
        "error": "Require Authorization"
      }
    },
    "response": null
  }
 *
 */

/**
 * @apiDefine applicationError
 * @apiErrorExample {json} Application Error
 HTTP/1.1 400 Bad Request
 {
    "errors": {
      "code": codeNumber,
      "content": {
        "error": "friendly message"
      }
    },
    "response": null
  }
 *
 */
// API endpoints.

router.all('*', _services.authMidleware);
router.use('/status', require('./server'));
router.use('/auth', require('./auth'));
router.use('/users', require('./users'));
router.use('/logs', require('./logs'));
router.use('/templates', require('./templates'));
var _default = router;
exports.default = _default;
module.exports = exports.default;