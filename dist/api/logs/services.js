"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _model = _interopRequireDefault(require("./model.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const LogServices = {
  saveLog(id, action, model, description) {
    new _model.default({
      author: id,
      actionType: action,
      model,
      description
    }).save();
  }

};
var _default = LogServices;
exports.default = _default;
module.exports = exports.default;