"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

class ServerActions {
  isRunning(req, res) {
    res.ok({
      message: 'Up and running'
    });
  }

}

exports.default = ServerActions;
module.exports = exports.default;