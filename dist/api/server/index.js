"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _actions = _interopRequireDefault(require("./actions"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = new _express.Router();
const actions = new _actions.default(); // Get methods

router.get('/', actions.isRunning);
var _default = router;
exports.default = _default;
module.exports = exports.default;