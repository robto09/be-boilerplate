"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.SchemaName = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _db = require("services/db");

var _model = _interopRequireWildcard(require("api/shared/user-driver/model"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const Schema = _mongoose.default.Schema;
const SchemaName = 'User';
exports.SchemaName = SchemaName;
const sharedModelProps = (0, _model.default)(SchemaName);
const paymentSchema = new Schema({
  cardHolderName: {
    type: String
  },
  cardNumber: {
    type: String
  },
  id: {
    type: Number
  },
  // cvv            : { type : String },
  expirationDate: {
    type: String
  },
  favorite: {
    type: Boolean
  },
  cardVendor: {
    type: String
  }
});
const userSchema = new Schema(_objectSpread({}, sharedModelProps, {
  facebookId: String,
  settings: Object,
  // TODO unused?
  favoriteCardId: String,
  payed: {
    type: Boolean,
    default: true
  },
  payment: [paymentSchema]
}), {
  timestamps: true,
  toObject: {
    getters: true,
    setters: true,
    virtuals: true
  }
});
const schema = {
  type: 'object',
  properties: _objectSpread({
    facebookId: {
      type: 'string'
    },
    favoriteCardId: {
      type: 'string'
    },
    payed: {
      type: 'boolean'
    },
    payment: {
      type: 'object',
      properties: {
        cardHolderName: {
          type: 'string'
        },
        cardNumber: {
          type: 'string'
        },
        id: {
          type: 'number'
        },
        expirationDate: {
          type: 'string'
        },
        favorite: {
          type: 'boolean'
        },
        cardVendor: {
          type: 'string'
        }
      }
    }
  }, _model.sharedValidationSchema),
  required: []
};
userSchema.virtual('fullName').get(_model.virtuals.getName);

userSchema.statics.validateSchema = (obj, requiredFields) => {
  const schemaReference = Object.assign({}, schema);

  if (requiredFields) {
    schemaReference.required = requiredFields;
  }

  return (0, _db.validateScheme)(obj, schemaReference);
};

userSchema.index({
  'phone': 1,
  email: 1
}, {
  background: true,
  partialFilterExpression: {
    'phone': {
      $exists: true
    },
    email: {
      $exists: true
    }
  }
}, (err, result) => {
  console.log('error creating index', err);
  console.log('result', result);
});

userSchema.statics.excludeFields = () => '-updatedAt -createdAt -__v -password -creditCards';

userSchema.statics.restrictedFields = () => ({
  values: 'firstName lastName',
  justWithParams: true
});

userSchema.statics.getHistoric = () => 'firstName lastName avatarUrl';

userSchema.pre('save', function hashPass(next) {
  _model.preSaveTrigger.call(this, next);
});
userSchema.pre('findOneAndUpdate', function updateTrigger(next) {
  _model.findOneAndUpdateTrigger.call(this, next);
});

userSchema.methods.comparePassword = function compareValidator(userPassword) {
  return _model.comparePassword.call(this, userPassword);
};

var _default = _mongoose.default.model(SchemaName, userSchema);

exports.default = _default;