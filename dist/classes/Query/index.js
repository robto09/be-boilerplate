"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Query = void 0;

var _db = require("services/db");

var _errors = require("services/errors");

var _defaultMessages = require("services/defaultMessages");

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class Query {
  constructor(model) {
    _defineProperty(this, "getById", (req, res) => {
      try {
        const {
          id
        } = req.params;
        const fieldsSelection = this.Model.excludeFields ? this.Model.excludeFields() : null;
        (0, _db.findById)(id, this.Model, fieldsSelection).then(doc => res.ok({
          message: (0, _defaultMessages.found)(this.collectionName),
          data: doc
        }), () => res.notFound((0, _errors.elementNotFound)(this.collectionName)));
      } catch (err) {
        console.log('error in findById', err);
        res.badRequest(err);
      }
    });

    _defineProperty(this, "findByQuery", (req, res, populateOpt = {}) => {
      const query = req.query;
      const {
        values: publicFields,
        justWithParams
      } = this.Model.restrictedFields ? this.Model.restrictedFields() : {};

      const {
        limit,
        offset,
        sort,
        fields,
        restricted
      } = query,
            QueryCriteria = _objectWithoutProperties(query, ["limit", "offset", "sort", "fields", "restricted"]);

      const queryOpt = (0, _db.getQueryOpt)(limit, offset, sort);

      try {
        if (restricted && !_lodash.default.isEmpty(publicFields) || !restricted) {
          if ((0, _db.checkRestricted)(req.params, justWithParams, restricted)) {
            const resultFields = restricted ? publicFields : (0, _db.getQueryFields)(fields);
            (0, _db.findInCollectionByQuery)(this.Model, QueryCriteria, resultFields, queryOpt, populateOpt).then(docs => res.ok({
              message: (0, _defaultMessages.found)(this.collectionName, docs),
              data: docs
            }), err => {
              console.log('err', err);
              res.notFound((0, _errors.elementNotFound)(this.collectionName));
            });
          } else {
            res.badRequest((0, _errors.unAuthorized)('Require Authorization'));
          }
        } else {
          throw _errors.withoutRestricted;
        }
      } catch (err) {
        console.log('err', err);
        res.badRequest(err);
      }
    });

    _defineProperty(this, "getByQuery", (req, res) => {
      this.findByQuery(req, res);
    });

    _defineProperty(this, "getAllData", (req, res, queryOpt) => {
      (0, _db.getAll)(this.Model, ...queryOpt).then(docs => res.ok({
        message: (0, _defaultMessages.found)(this.collectionName, docs),
        data: docs
      }), () => res.badRequest((0, _errors.elementNotFound)(this.collectionName)));
    });

    this.Model = model;
    this.collectionName = this.Model.collection.collectionName;
  }

}

exports.Query = Query;