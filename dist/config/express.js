"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _compression = _interopRequireDefault(require("compression"));

var _morgan = _interopRequireDefault(require("morgan"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _helmet = _interopRequireDefault(require("helmet"));

var _cors = _interopRequireDefault(require("cors"));

var _expressQueryBoolean = _interopRequireDefault(require("express-query-boolean"));

var _responses = _interopRequireDefault(require("./responses"));

var _connectMultiparty = _interopRequireDefault(require("connect-multiparty"));

var _requestIp = _interopRequireDefault(require("request-ip"));

var _environment = require("./environment");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = app => {
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('X-powered-by', 'DNAMIC Software');
    next();
  });

  if (_environment.env !== 'production') {
    app.use((0, _helmet.default)());
    app.use('/docs', _express.default.static(`${__dirname}/../../docs`));
    app.use((0, _morgan.default)('dev'));
  } else {
    app.use((0, _helmet.default)({
      hsts: false
    }));
  }

  app.use(_requestIp.default.mw());
  app.use((0, _connectMultiparty.default)());
  app.use((0, _cors.default)());
  app.use((0, _compression.default)());
  app.use((0, _responses.default)());
  app.use(_bodyParser.default.json({
    limit: '100mb'
  }));
  app.use(_bodyParser.default.urlencoded({
    limit: '100mb',
    extended: true
  }));
  app.use((0, _expressQueryBoolean.default)());
};

exports.default = _default;
module.exports = exports.default;