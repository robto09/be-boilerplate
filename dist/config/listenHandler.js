"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _chalk = _interopRequireDefault(require("chalk"));

var _server = _interopRequireDefault(require("server"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = (err, port) => {
  if (err) console.log(err);
  const msg = `API server listening on port ${port} in ${_server.default.get('env')} mode...`;
  console.log(_chalk.default.white(JSON.stringify({
    msg
  })));
};

exports.default = _default;
module.exports = exports.default;