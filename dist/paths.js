"use strict";

const path = require('path');

const appDirectory = path.resolve(__dirname, '../');

const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

module.exports = {
  postman: resolveApp('postman'),
  packageJson: resolveApp('package.json')
};