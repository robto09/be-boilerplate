"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = (0, _express.default)();

require('config/db');

require('config/express')(app);

require('config/routes')(app);

var _default = app;
exports.default = _default;
module.exports = exports.default;