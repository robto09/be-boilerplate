"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _nodemailer = _interopRequireDefault(require("nodemailer"));

var _lodash = _interopRequireDefault(require("lodash"));

var _qrcode = _interopRequireDefault(require("qrcode"));

var _jszip = _interopRequireDefault(require("jszip"));

var _awsInit = require("./aws-init");

var _constants = _interopRequireDefault(require("../constants"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Third party modules
// In house modules
const {
  AWS_EMAIL_SENDER: from
} = _constants.default;

const transporter = _nodemailer.default.createTransport({
  SES: _awsInit.SES
});

const Mailer = {
  sendMail(email, subject, description) {
    const message = {
      from,
      to: email,
      subject: subject,
      html: description
    };
    transporter.sendMail(message, error => {
      if (error) {
        console.log('sendMail error: ', error);
      }
    });
  },

  async sendMailWithCodes(email, subject, codes) {
    const zip = new _jszip.default();
    let images = 0;

    _lodash.default.forEach(codes, function generateImages(code) {
      _qrcode.default.toDataURL(code.code, function generateImage(err, url) {
        images++;
        zip.file(`${code._id}.png`, url.replace(/^data:image\/(png|jpg);base64,/, ''), {
          base64: true
        });

        if (images === codes.length) {
          zip.generateAsync({
            type: 'uint8array'
          }).then(function generateZip(zipFile) {
            const message = {
              from,
              to: email,
              subject: subject,
              attachments: [{
                filename: 'qrcodes.zip',
                content: zipFile
              }]
            };
            transporter.sendMail(message, function errorHandler(error) {
              if (error) {
                console.log('sendMail error: ', error);
              }
            });
          });
        }
      });
    });
  }

};
var _default = Mailer;
exports.default = _default;
module.exports = exports.default;