"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _awsInit = require("./aws-init");

// In house modules
const awsNotification = {
  sendSms: (PhoneNumber, Message, Subject) => new Promise((resolve, reject) => {
    const params = {
      PhoneNumber,
      Message,
      Subject,
      MessageStructure: 'string'
    };

    _awsInit.SNS.publish(params, (err, data) => {
      if (err) reject(err);
      resolve(data);
    });
  })
};
var _default = awsNotification;
exports.default = _default;
module.exports = exports.default;