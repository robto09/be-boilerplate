"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _ses = require("services/aws/ses");

var _constants = _interopRequireDefault(require("services/constants"));

var _helpers = require("services/helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

const {
  AWS_TEMPLATES: {
    CONFIRM_EMAIL,
    DOCS_VERIFICATION,
    FORGOT_PASSWORD,
    JOURNEY,
    PAYMENT_ERROR,
    RESEND_VERIFICATION,
    WELCOME
  },
  TRANSLATIONS: {
    EMAILS: {
      SUBJECTS,
      ERROR_EMAIL
    }
  }
} = _constants.default;

const sendEmailByTemplate = template => (destination, variables) => (0, _ses.sendTemplateMail)(destination, template, variables);

const sendRawEmail = (destination, subject, message) => (0, _ses.sendEmail)(destination, subject, message);

const multilanguageParams = (multiLangSub, variables) => {
  const {
    language
  } = variables,
        emailVariables = _objectWithoutProperties(variables, ["language"]);

  return _objectSpread({
    subject: (0, _helpers.getLang)(multiLangSub, language),
    languageCode: {
      [language]: true
    }
  }, emailVariables);
};

var _default = {
  confirmEmail: (destination, variables) => {
    const wholeVariables = multilanguageParams(SUBJECTS.CONFIRM_EMAIL, variables);
    return sendEmailByTemplate(CONFIRM_EMAIL)(destination, wholeVariables);
  },
  docsVerifiedEmail: (destination, variables) => {
    const wholeVariables = multilanguageParams(SUBJECTS.DOCS_VERIFICATION, variables);
    return sendEmailByTemplate(DOCS_VERIFICATION)(destination, wholeVariables);
  },
  forgotPassEmail: (destination, variables) => {
    const wholeVariables = multilanguageParams(SUBJECTS.FORGOT_PASSWORD, variables);
    return sendEmailByTemplate(FORGOT_PASSWORD)(destination, wholeVariables);
  },
  paymentErrorEmail: (destination, variables) => sendEmailByTemplate(PAYMENT_ERROR)(destination, variables),
  receiptEmail: (destination, variables) => sendEmailByTemplate(JOURNEY)(destination, variables),
  resendVerificationEmail: (destination, variables) => {
    const wholeVariables = multilanguageParams(SUBJECTS.RESEND_VERIFICATION, variables);
    return sendEmailByTemplate(RESEND_VERIFICATION)(destination, wholeVariables);
  },
  welcomeEmail: (destination, variables) => {
    const wholeVariables = multilanguageParams(SUBJECTS.WELCOME, variables);
    return sendEmailByTemplate(WELCOME)(destination, wholeVariables);
  },
  rawErrorEmail: lang => (destination, rideId) => {
    const emailParams = ERROR_EMAIL(lang); // eslint-disable-line new-cap

    return sendRawEmail(destination, `${emailParams.SUBJECT} #${rideId}`, emailParams.COPY);
  },
  rawEmail: (destination, subject, message) => sendRawEmail(subject, destination, message)
};
exports.default = _default;
module.exports = exports.default;