"use strict";

var _autocannon = _interopRequireDefault(require("autocannon"));

var _environment = require("config/environment");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const argv = require('minimist')(process.argv.slice(2));

const {
  u: endpoint,
  c: connections = 100
} = argv;
const instance = (0, _autocannon.default)({
  url: `${_environment.envUrl}/${endpoint}`,
  connections,
  pipelining: 1,
  // default
  duration: 10 // default

}, (err, results) => {
  // eslint-disable-line
  console.log('results', results);

  if (err) {
    console.log('benchmark error');
  }
});

_autocannon.default.track(instance); // this is used to kill the instance on CTRL-C


process.once('SIGINT', () => {
  instance.stop();
});