const utils = require('./utils');
const {postman} = require('../src/paths');

// ENDPOINTS
const server = require('./endpoints/server.json');
const users = require('./endpoints/users.json');
const auth = require('./endpoints/auth.json');
const notifications = require('./endpoints/notifications.json');
const emailTemplates = require('./endpoints/emailTemplates.json');

const endpoints = [
  {name : 'Server', endpoints : server.endpoints},
  {name : 'Users', endpoints : users.endpoints},
  {name : 'Auth', endpoints : auth.endpoints},
  {name : 'Notifications', endpoints : notifications.endpoints},
  {name : 'Email Templates', endpoints : emailTemplates.endpoints},
];

const collectionInput =  utils.generateCollectionFolders(endpoints);
const destination = `${__dirname}/collections`;

utils.generateCollection(collectionInput, destination);
utils.getEndpointsData(`${postman}/data`).then((endpointsData) => {
  utils.generateEnvironment('local', endpointsData, destination);
  utils.generateEnvironment('development', endpointsData, destination);
  utils.generateEnvironment('dev-stable', endpointsData, destination);
  utils.generateEnvironment('staging', endpointsData, destination);
  utils.generateEnvironment('production', endpointsData, destination);
}, () => {
  console.log('error creating your environment data');
});
