const fs = require('fs');
const jyson = require('jyson');
const pretest = require('../scripts/pretest');
const {packageJson: {version}} = require('../../server/files');

const postmanEnvironmentTemplate = jyson.buildTemplateFunction({
  id     : 'id',
  name   : 'name',
  values : ['environment.values.$'],
});

const postmanCollectionTemplate = jyson.buildTemplateFunction({
  info : {
    name        : 'info.name',
    _postman_id : 'info._postman_id',
    description : 'info.description',
    schema      : 'info.schema',
  },
  item  : ['api.endpoints.$'],
  event : ['api.events.$'],
});


const utils = {
  readCreate : (path, data, message) => {

    const customMessage = fs.existsSync(path) ? `${message} updated` : `${message} created`;

    fs.writeFile(path, data, 'utf8',  (err) => { //eslint-disable-line
      if (err) {
        return console.log(err);
      }
      console.log(customMessage);
    });
  },
  readFilesInFolder : (folder) => new Promise( (resolve, reject) => {
    const fileNames = fs.readdirSync(folder);
    if (!fileNames.length) reject(new Error());
    const filePromises = [];
    fileNames.forEach( (fileName) => {
      const filePromise = new Promise((res, rej) => {
        fs.readFile(`${folder}/${fileName}`, (err, data) => {
          if (err || !data) {
            rej(err);
          }
          const jsonObj = JSON.parse(data);
          return res(jsonObj);
        });

      });
      filePromises.push(filePromise);
    });

    Promise.all(filePromises).then(values => resolve(values)).catch((error) => reject(error));

  }),
  getEndpointsData : function getEndpointsData (folder) { //eslint-disable-line
    try  {
      return this.readFilesInFolder(folder)
        .then( data => data.reduce((valInitial, valActual) => {
          valInitial.push(...valActual.data);
          return valInitial;
        }, []),
        (err) => { throw err; });
    } catch (err) {
      console.log(`error reading environment data ${err}`);
    }
  },
  generateFolderDefault : (endpoint) => ({
    name        : endpoint.name,
    description : `${endpoint.name} endpoints`,
    item        : [
      ...endpoint.endpoints,
    ],
  }),
  generateCollectionFolders : function generateCollectionFolders (endpointsData)  {
    const endpoints = endpointsData.map((endpoint) => {
      const endpointsStructure = this.generateFolderDefault(endpoint);
      const subFolders = endpoint.subfolders;
      if (subFolders && subFolders.length) {
        const subFoldersStructure = subFolders.map((subEndpoint) => {
          const defaultStructure = this.generateFolderDefault(subEndpoint);
          defaultStructure._postman_isSubFolder = true;
          return defaultStructure;
        });
        endpointsStructure.item = [...endpointsStructure.item, ...subFoldersStructure];
      }
      return endpointsStructure;
    });

    return ({
      endpoints,
    });
  },
  generateEnvironment : function generateEnvironment (env, endpointsData, location) {
    let url = '';

    switch (env) {
    case 'local':
      url = `http://localhost:1102/${version}`;
      break;
    case 'development':
      url = `https://dev.alltruckcr.com/${version}`;
      break;
    case 'dev-stable':
      url = `https://dev-stable.alltruckcr.com/${version}`;
      break;
    case 'staging':
      url = `https://staging.alltruckcr.com/${version}`;
      break;
    case 'production':
      url = `https://api.alltruckcr.com/${version}`;
      break;
    default:
      return false;
    }

    const data = {
      id          : 'ae1458a8',
      name        : `All_Truck_${env}_Environment`,
      environment : {
        values : [
          {
            'enabled' : true,
            'key'     : 'url',
            'value'   : url,
            'type'    : 'text',
          },
          ...endpointsData,
        ],
      },
    };
    const jsonData =  JSON.stringify(postmanEnvironmentTemplate(data), null, 2);
    return this.readCreate(`${location}/All_Truck_${env}_Environment.json`, jsonData, `Postman ${env} Environment`);
  },
  generateCollectionEvents : () => [
    {
      'listen' : 'prerequest',
      'script' : {
        'id'   : '394e936f-8124-45ed-bbc1-f0d16fccd115',
        'type' : 'text/javascript',
        'exec' : [
          'pm.globals.set("loadUtils",',
          ...pretest,
          '+ "; loadUtils();");',
        ],
      },
    },
    {
      'listen' : 'test',
      'script' : {
        'id'   : 'a03efcd5-26c2-4eaf-952f-a8765bee950f',
        'type' : 'text/javascript',
        'exec' : [
          '',
        ],
      },
    },
  ],
  generateCollection : function generateCollection (collectionInput, location)  {

    const inputDefault = {
      info : {
        name        : 'All_Truck',
        _postman_id : '464d0c69-277e-70fd-e579-8b8b0976ad03',
        description : 'All truck Endpoints',
        schema      : 'https://schema.getpostman.com/json/collection/v2.1.0/collection.json',
      },
      api : {
        endpoints : collectionInput.endpoints,
        events    : this.generateCollectionEvents(),
      },
    };

    const collectionData = JSON.stringify(postmanCollectionTemplate(inputDefault), null, 2);
    utils.readCreate( `${location}/All_Truck.postman_collection.json`, collectionData, 'Postman Collection');
  },
};


module.exports = utils;
