const chalk = require('chalk');
const fs = require('fs');

console.log(chalk.blue('Preparing the environment...'));
fs.createReadStream('env.txt').pipe(fs.createWriteStream('.env'));

console.log(chalk.green('Done, create something cool!'));
