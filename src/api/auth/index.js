import { Router } from 'express';
import Actions from './actions';

const router = new Router();
const actions = new Actions();

// Post methods
router.post('/user', actions.user);

// Get methods
router.get('/token', actions.refresh);

export default router;
