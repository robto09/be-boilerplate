// Third party modules
import _ from 'lodash';

// In house modules
import { saveLog } from 'api/logs/services';
import { issueToken, verifyToken } from 'api/auth/services';
import notificationServices from 'services/notifications';

import { isDisposableEmail, shortUrl, decimalFlagsToResponseCode, flagActive,  difference } from 'services/services'; // eslint-disable-line

import {
  emailNotExist, emailBlacklisted, // eslint-disable-line
  emailAlreadyExists, emailWasVerified,
  emailPhoneNotExist,
  phoneNotVerified, phoneWasVerified,
  passRequired,
  invalidToken, unAuthorized,
  notFound, badRequest,
  phoneAlreadyExists, phoneAndEmailExists,
  internalError,
} from 'services/errors';
import {
  findById,
  deleteById,
} from 'services/db';
import { deleted, passUpdated, errorDeleting } from 'services/defaultMessages';
import Constants from 'services/constants';
import { env } from 'config/environment';

const {
  emails : { welcomeEmail, resendVerificationEmail, forgotPassEmail },
  sms : { confirmSms, sendVerificationSms },
} = notificationServices;
const {
  CMS_URL,
  LOG_ACTION_TYPES : { UPDATE, DELETE },
  TOKENS_TIME: { ACTIVATE, FORGOT },
} = Constants;

const cmsUrl = CMS_URL(env); // eslint-disable-line new-cap

export default ({

  delete : async (req, res, Model) => {
    const collectionName = Model.collection.collectionName;
    const { id } = req.params;
    deleteById(id, Model).then((user) => {
      saveLog(user._id, DELETE, collectionName, deleted(collectionName));
      res.ok({ message : deleted(collectionName) });
    }, () => res.badRequest(notFound(errorDeleting(collectionName))));
  },
  getProfile : async (req, res, Model) => {
    const { userId } = req.user;
    if (!userId) res.badRequest(invalidToken);
    findById(userId, Model, Model.excludeFields().replace('-creditCards', ''))
      .then( (userObj) => res.ok({ 'data' : userObj }),
        () => res.notFound(notFound('Failed to retrieve profile')));
  },
  verify : async (req, res, Model) => {
    const { token } = req.params;
    const collectionName = Model.collection.collectionName;
    try {
      const tokenInfo = await verifyToken(token);
      const { content: { mail: email } } = tokenInfo;
      const verifiedDriver = await Model.findOneAndUpdate({ email }, { emailVerified : true }, { new : true });
      if (verifiedDriver) {
        const variables = {
          user     : verifiedDriver.fullName,
          language : verifiedDriver.language,
        };
        welcomeEmail(verifiedDriver.email, variables);
        saveLog(verifiedDriver.id, UPDATE, collectionName, `${collectionName} element verified`);
        const verifiedPagesSuccess = `${cmsUrl}/validateEmail/success`;

        res.redirect(verifiedPagesSuccess);


      } else {
        const verifiedPagesError = `${cmsUrl}/validateEmail/error`;
        res.redirect(verifiedPagesError);
      }
    } catch (err) {
      const msg = err.message || 'Failed to verify your account';
      res.unauthorized(unAuthorized(msg));
    }
  },
  verifyPhoneCode : async (req, res, model) => {
    try {
      const { phoneCode } = req.body;
      const { token } = req.params;
      if (!phoneCode) throw badRequest('No phone verification code provided');

      await verifyToken(token).then((tokenInfo) => {
        const { content: { phone, phoneCode: code } } = tokenInfo;
        if (parseInt(phoneCode, 10) !== code) throw badRequest('Phone code does not match');

        const update = { phoneVerified : true, verifiedAt : Date.now() };
        model.findOneAndUpdate({ phone }, update, (err, user) => {
          if (err || !user) {
            res.notFound(notFound('Failed to verify yor phone number'));
          } else {
            res.ok({ message : 'Phone successfully verified' });
          }
        });

      }, (error) => res.badRequest(badRequest(error.message, error)));

    } catch (err) {
      res.badRequest(err);
    }
  },
  forgotPassword : (req, res, Model ) => {
    const { input } = req.params;
    const collectionName = Model.collection.collectionName;
    const forgotUrl = (token) => `${cmsUrl}/${collectionName}/reset/?token=${token}`;

    Model.findOne({ $or : [{ 'phone' : input }, { email : input }] }, async (err, driver) => {
      try {
        if (err || !driver) throw emailPhoneNotExist;
        const { _id: userId, phone, email, phoneVerified, language } = driver;

        if (phone === input) {
          if (!phoneVerified) throw phoneNotVerified;

          const token = await issueToken({ userId, phone }, FORGOT);
          const urlShorten = await shortUrl(forgotUrl(token));
          confirmSms(phone, urlShorten)
            .then((data) => res.ok({ message : 'Sms send', token, ...data }),
              () => res.badRequest(internalError('internal error sending your verification')));
        }

        if (email === input) {

          const token = issueToken({ userId, mail : email }, FORGOT);
          const urlShorten = await shortUrl(forgotUrl(token));

          const variables = {
            language,
            linkUrl : urlShorten, // CURRENT_ENV
          };
          forgotPassEmail(email, variables);
          res.ok({ message : 'check your email' });
        }
      } catch (error) {
        res.badRequest(error);
      }
    });

  },
  sendVerification : async (req, res, Model) => {
    const { phone } = req.params;
    const { language } = req.query;
    Model.findOne({ phone }, async (err, driver) => {
      try {
        if (err) throw internalError('internal error with db');
        if (driver) throw phoneAlreadyExists;
        sendVerificationSms({}, phone, language)
          .then((tokenResponse) => res.ok({ message : 'Sms send', token : tokenResponse }),
            () => res.badRequest(internalError('internal error sending your verification')));
      } catch (error) {
        res.badRequest(error);
      }
    });
  },

  resendVerification : (req, res, Model) => {
    const collectionName = Model.collection.collectionName;
    const environmentUrl = req.environmentUrl();
    const { input } = req.params;

    Model.findOne({ $or : [{ phone : input }, { email : input }] }, async (err, driver) => {
      try {
        if (err || !driver) throw emailPhoneNotExist;

        if (driver.phone === input) {
          const { _id: driverId, phone, language, phoneVerified } = driver;
          if (phoneVerified) throw phoneWasVerified;

          await sendVerificationSms({ driverId }, phone, language)
            .then((tokenResponse) => res.ok({ message : 'Sms send', token : tokenResponse }),
              () => res.badRequest(internalError('internal error sending your verification')));

        }

        if (driver.email === input) {
          const { _id: driverId, email: mail, language, emailVerified } = driver;
          if (emailVerified) throw emailWasVerified;
          const token = await issueToken({ driverId, mail }, ACTIVATE);
          const variables = {
            language,
            linkUrl : `${environmentUrl}/${collectionName}/verify/${token}`,
          };
          resendVerificationEmail(mail, variables);
          res.ok({ message : 'check your email' });
        }

      } catch (error) {
        res.badRequest(error);
      }
    });
  },
  changePasswordForgot : async (req, res, Model) => {
    const collectionName = Model.collection.collectionName;
    try {
      const { password } = req.body;
      if (!password) throw passRequired;
      const { token } = req.params;
      const tokenInfo = await verifyToken(token);
      const { content: { phone, mail } } = tokenInfo;
      const successMessage = passUpdated(collectionName);
      if (phone) {
        await Model.findOne({ phone }, async (err, driver) => {
          if (err && !driver) res.badRequest(notFound('Phone number not found'));
          driver.password = password;
          await driver.save();
          saveLog(driver.id, UPDATE, collectionName, successMessage);
          res.ok({ message : 'Password successfully changed' });
        });
      } else {
        const driver = await Model.findOne({ email : mail });
        if (!driver) throw emailNotExist;

        driver.password = password;
        await driver.save();
        saveLog(driver.id, UPDATE, collectionName, successMessage);
        res.ok({ message : successMessage });
      }

    } catch (err) {
      const message = err.message || 'Error updating password';
      res.badRequest(Object.assign(err, message));
    }
  },
  passwordById : async (req, res, Model) => {
    const collectionName = Model.collection.collectionName;
    try {
      const { password } = req.body;
      if (!password) throw passRequired;
      const { id } = req.params;
      const { userId } = req.user;
      const updatedDriver = await Model.findOne({ '_id' : id });
      updatedDriver.password = password;
      await updatedDriver.save();
      saveLog(userId, UPDATE, collectionName, `${collectionName} password updated`);
      res.ok({ message : `${collectionName} password successfully updated` });
    } catch (err) {
      const message = err.message || 'Error updating password';
      res.badRequest(message);
    }
  },
  emailPhoneUsed : (email, phone, Model) => new Promise( ( resolve, reject) => {

    Model.findOne({ $or : [ { email }, { phone } ] }).then((inUse) => {

      if (inUse) {
        const { email : inUseEmail, phone : phoneInUse } = inUse;
        const emailUsed = inUseEmail === email;
        const phoneUsed = phoneInUse === phone;

        if (emailUsed && phoneUsed) reject(phoneAndEmailExists);
        if (emailUsed) reject(emailAlreadyExists);
        if (phoneUsed) reject(phoneAlreadyExists);

      }

      resolve();

    }, () => resolve());
  }),
  checkDuplicate : function checkDuplicate (req, res, Model) {
    const { email, phone } = req.query;
    const formattedPhone = `+${_.trim(phone)}`;
    return this.emailPhoneUsed(email, formattedPhone, Model)
      .then(() => res.ok({ message : 'Email and phone available' }),
        (error) => res.badRequest(error));
  },

});

