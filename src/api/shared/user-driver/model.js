import bcrypt from 'bcryptjs';
import { uniqueness } from 'services/validators';
import { fields } from 'services/db';

import {
  invalidCredentials,
} from 'services/errors';

export const hashPassword = (password) => new Promise((resolve, reject) => {
  bcrypt.genSalt(10, (error, salt) => {
    if (error) reject(error);
    bcrypt.hash(password, salt, (err, hash) => {
      if (err) reject(err);
      resolve(hash);
    });
  });
});

export const virtuals = {
  getName : function getName () { // this refer to doc dont change to arrow funct
    return `${this.firstName} ${this.lastName}`;
  },
};

export async function preSaveTrigger (next) {
  // Generate a salt and apply to a password with the hashed one

  if (!this.isModified('password')) return next();
  // Only hash the password if it has been modified (or is new)
  await hashPassword(this.password).then((hashedPass) => {
    this.password = hashedPass;
  }, (error) => next(error));
  return next();
}
export async function findOneAndUpdateTrigger (next) {
  const update = this.getUpdate();
  const { password, phone, email } = update.$set;
  if (password) {
    await hashPassword(this.password).then((hashedPass) => {
      this._update.password = hashedPass;
    });
  }
  if (phone) {
    this._update.phoneVerified = false;
  }
  if (email) {
    this._update.emailVerified = false;
  }
  next();
}

export function comparePassword (userPassword) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(userPassword, this.password, (err, match) => {
      if (match === false) {
        reject(invalidCredentials);
      }
      resolve(match);
    });
  });
}


export function verifyEmail (SchemaName, value, done) {
  if (!this.isModified('email')) {
    return done(true);
  }
  return uniqueness(SchemaName, { email : value }, done);
}

export function verifyPhone (SchemaName, value, done) {
  if (!this.isModified('phone')) {
    return done(true);
  }
  // value.phone because we are validating whole others obj
  return uniqueness(SchemaName, { 'phone' : value }, done);
}

export const sharedValidationSchema = {
  firstName : { type : 'string' },
  lastName  : { type : 'string' },
  email     : { type : 'string', 'format' : 'email' },
  phone     : { type : 'string' },
  password  : { type : 'string' },
  area      : { type : 'string' },
  language  : { type : 'string' },
  others    : {
    type : 'object',
  },
  device : {
    type       : 'object',
    properties : {
      appVersion : { type : 'string' },
      brand      : { type : 'string' },
      model      : { type : 'string' },
      os         : { type : 'string' },
      osVersion  : { type : 'string' },
    },
  },
  playerId       : { type : 'string' },
  avatarUrl      : { type : 'string' },
  deviceId       : { type : 'string' },
  verifiedAt     : { type : 'string' },
  rating         : { type : 'number', minimum : 0 },
  phoneVerified  : { type : 'boolean' },
  emailVerified  : { type : 'boolean' },
  creditCards    : { type : 'array', items : [ { type : 'object' }] },
  isActive       : { type : 'boolean' },
  inactiveReason : {
    type       : 'object',
    properties : {
      id          : { type : 'string' },
      description : { type : 'string' },
    },
  },
};

export default (SchemaName) => ({
  firstName : { type : String, trim : true },
  lastName  : { type : String, trim : true },
  email     : {
    type      : String,
    lowercase : true,
    trim      : true,
    required  : true,
    validate  : {
      isAsync   : true,
      validator : function emailValidator (value, done) {
        return verifyEmail.call(this, SchemaName, value, done);
      },
      message : 'Email already in used',
    },
  },
  password : { type : String, required : true, trim : true },
  country  : { type : String, trim : true, uppercase : true },
  region   : { type : String, trim : true },
  language : { type : String, trim : true, uppercase : true },
  phone    : {
    type     : String,
    trim     : true,
    required : true,
    validate : {
      isAsync   : true,
      validator : function phoneValidator (value, done) {
        return verifyPhone.call(this, SchemaName, value, done);
      },
      message : 'Phone already in used',
    },
  },
  device : {
    appVersion : String,
    brand      : String,
    model      : String,
    os         : String,
    osVersion  : String,
  },
  playerId       : String,
  avatarUrl      : String,
  deviceId       : { type : String },
  verifiedAt     : Date,
  rating         : { ...fields.positiveNumber, default : 5 },
  phoneVerified  : { type : Boolean, default : false }, // phone verified
  emailVerified  : { type : Boolean, default : false },
  isActive       : { type : Boolean, default : true }, // handle bans
  inactiveReason : {
    id          : String,
    description : String,
  },
});

