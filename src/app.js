import { port } from 'config/environment';
import listenHandler from 'config/listenHandler';
import app from './server';
app.listen(port, (err) => listenHandler(err, port));
export default app;
