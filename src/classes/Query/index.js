import {
  getQueryOpt, getQueryFields,
  findById, getAll,
  findInCollectionByQuery,
  checkRestricted,
} from 'services/db';
import { elementNotFound, withoutRestricted, unAuthorized } from 'services/errors';
import { found } from 'services/defaultMessages';
import _ from 'lodash';

export class Query {

  constructor (model) {
    this.Model = model;
    this.collectionName = this.Model.collection.collectionName;
  }

  getById = (req, res) => {
    try {
      const { id } = req.params;
      const fieldsSelection = this.Model.excludeFields ? this.Model.excludeFields() : null;
      findById(id, this.Model, fieldsSelection).then(
        (doc) => res.ok({ message : found(this.collectionName), data : doc }),
        () => res.notFound(elementNotFound(this.collectionName)));
    } catch (err) {
      console.log('error in findById', err);
      res.badRequest(err);
    }
  }

  findByQuery = (req, res, populateOpt = {}) => {
    const query = req.query;
    const { values: publicFields, justWithParams } = this.Model.restrictedFields ? this.Model.restrictedFields() : {};
    const { limit, offset, sort, fields, restricted, ...QueryCriteria } = query;
    const queryOpt = getQueryOpt(limit, offset, sort);
    try {
      if (restricted && !_.isEmpty(publicFields) || !restricted) {
        if (checkRestricted(req.params, justWithParams, restricted)) {
          const resultFields = restricted ? publicFields : getQueryFields(fields);
          findInCollectionByQuery(this.Model, QueryCriteria, resultFields, queryOpt, populateOpt)
            .then((docs) => res.ok({ message : found(this.collectionName, docs), data : docs }),
              (err) => {
                console.log('err', err);
                res.notFound(elementNotFound(this.collectionName));
              });
        } else {
          res.badRequest(unAuthorized('Require Authorization'));
        }
      } else {
        throw withoutRestricted;
      }
    } catch (err) {
      console.log('err', err);
      res.badRequest(err);
    }
  }

  getByQuery = (req, res) => {
    this.findByQuery(req, res);
  }

  getAllData = (req, res, queryOpt) => {
    getAll(this.Model, ...queryOpt)
      .then((docs) => res.ok({ message : found(this.collectionName, docs), data : docs }),
        () => res.badRequest(elementNotFound(this.collectionName)) );
  }
}
