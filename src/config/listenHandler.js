import chalk from 'chalk';
import app from 'server';
export default (err, port) => {
  if (err) console.log(err);
  const msg = `API server listening on port ${port} in ${app.get('env')} mode...`;
  console.log(chalk.white(JSON.stringify({ msg })));
};
