// Third party modules
import nodemailer from 'nodemailer';
import _ from 'lodash';
import QRCode from 'qrcode';
import JSZip from 'jszip';

// In house modules
import { SES } from './aws-init';
import Constants from '../constants';

const { AWS_EMAIL_SENDER : from } = Constants;
const transporter = nodemailer.createTransport({ SES });

const Mailer = {

  sendMail (email, subject, description) {
    const message = {
      from,
      to      : email,
      subject : subject,
      html    : description,
    };
    transporter.sendMail(message, (error) => {
      if (error) {
        console.log('sendMail error: ', error);
      }
    });
  },

  async sendMailWithCodes (email, subject, codes) {
    const zip = new JSZip();
    let images = 0;
    _.forEach(codes, function generateImages (code) {
      QRCode.toDataURL(code.code, function generateImage (err, url) {
        images ++;
        zip.file(`${code._id}.png`, url.replace(/^data:image\/(png|jpg);base64,/, ''), { base64 : true });
        if (images === codes.length) {
          zip.generateAsync({ type : 'uint8array' }).then(function generateZip (zipFile) {
            const message = {
              from,
              to          : email,
              subject     : subject,
              attachments : [
                {
                  filename : 'qrcodes.zip',
                  content  : zipFile,
                },
              ],
            };
            transporter.sendMail(message, function errorHandler (error) {
              if (error) {
                console.log('sendMail error: ', error);
              }
            });
          });
        }
      });
    });
  },
};

export default Mailer;
