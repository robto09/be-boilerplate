import { elementNotFound } from 'services/errors';

const queryServices = {
  getQueryOpt : (limit, offset, sort) => {
    const queryOpt = {};
    if (offset && !isNaN(offset)) {
      queryOpt.skip = parseInt(offset, 10);
    }
    if (limit && !isNaN(limit)) {
      queryOpt.limit = parseInt(limit, 10);
    }
    if (sort && typeof sort === 'string') {
      queryOpt.sort = sort;
    }
    return queryOpt;
  },
  getQueryFields : (fields) => {
    let resultFields = fields;
    if (fields && typeof fields === 'string') {
      resultFields = fields.split(',').join(' ');
    }
    return resultFields;
  },
  getAll : (model, fields = '', populateOptions = {}) => new Promise( (resolve, reject) => {

    let query = model.find({}, fields);
    query = queryServices.populateOpt(query, populateOptions);
    query.lean().exec().then( (docs) => {
      if (docs && !docs.length) reject(elementNotFound(model.collection.collectionName));
      resolve(docs);
    }).catch((error) => reject(error));

  }),
  findById : (id, model, fields = '', populateOptions = {}) => new Promise((resolve, reject) => {
    let query = model.findById(id, fields);
    query = queryServices.populateOpt(query, populateOptions);
    query.exec()
      .then( (doc) => {
        if (!doc) reject(elementNotFound(model.collection.collectionName));
        resolve(doc);
      }).catch(err => reject(err));
  }),
  populateOpt : (query, populateOptions) => {
    let output = query;
    if (Array.isArray(populateOptions)) {
      output = populateOptions.reduce((result, { name, fields }) => result.populate(name, fields), output);
    } else {
      if (Object.keys(populateOptions).length) {
        output = output.populate(populateOptions.name, populateOptions.fields);
      }
    }

    return output;
  },
  findInCollectionByQuery : (model, query, fields = null, queryOpt = {}, populateOptions = {}) => new Promise((resolve, reject) => {
    let queryBuilder = model.find(query, fields, queryOpt);
    queryBuilder = queryServices.populateOpt(queryBuilder, populateOptions);
    queryBuilder.exec().then((docs) => {
      if (docs && !docs.length) reject(elementNotFound(model.collection.collectionName));
      resolve(docs);
    }).catch((error) => reject(error));

  }),
  updateById : (model, id, body, fields = null, populateOptions = {}) => new Promise( (resolve, reject) => {
    let query = model.findByIdAndUpdate(id, body, { runValidators : true, context : 'query', runSettersOnQuery : true, new : true, select : fields });
    query = queryServices.populateOpt(query, populateOptions);
    query.exec()
      .then( (doc) => {
        if (!doc) reject(elementNotFound(model.collection.collectionName));
        resolve(doc.toObject({ getters : true }));
      })
      .catch((error) => reject(error));
  }),
  updateCollection : (model, query, fields = '', options = {}) => new Promise((resolve, reject) => {
    const dbQuery = model.update(query, fields, { multi : true, runValidators : true, ...options });
    dbQuery.lean().exec()
      .then( (docs) => resolve(docs)).catch((error) => reject(error));
  }),
  deleteById : (id, model) => new Promise( (resolve, reject) => {
    model.findByIdAndRemove(id, (err, doc) => {
      if (err || !doc) reject(err);
      resolve(doc);
    });
  }),
};

export default queryServices;
