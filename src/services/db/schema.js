import Ajv from 'ajv';
const ajv = new Ajv({ unknownFormats : 'ignore' });
require('ajv-keywords')(ajv, 'uniqueItemProperties');
import mongoose from 'mongoose';
const { Schema } = mongoose;

const schemaServices = {
  validateScheme : (obj, scheme) => new Promise((resolve, reject) => {
    const valid = ajv.validate(scheme, obj);
    if (!valid) reject(ajv.errors);
    resolve(valid);
  }),
  numberToString : (num) => num.toString(),
  stringToNumber : (decimal) => decimal && decimal._bsontype === 'Decimal128' ? JSON.parse(decimal) : decimal,
  castId         : (id) => mongoose.Types.ObjectId(id),
  idString       : (id) => id ? id.toString() : id,
};

schemaServices.fields = {
  idField        : { type : Schema.Types.ObjectId, get : schemaServices.idString, set : schemaServices.castId },
  positiveNumber : { type : Number, min : 0 },
  decimalField   : { type : Schema.Types.Decimal128, min : 0, set : schemaServices.numberToString, get : schemaServices.stringToNumber },
};

export default schemaServices;
