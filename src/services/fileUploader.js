// Third party modules
import S3fs from 's3fs';
import path from 'path';
import shortid from 'shortid';
import fs from 'fs';
import async from 'async';

// In house modules
import Constants from './constants';

const S3FS = new S3fs(Constants.BUCKET, {
  accessKeyId     : Constants.S3_ID,
  secretAccessKey : Constants.S3_SECRET,
});

const FilesActions = {
  upload : (file) => new Promise((resolve, reject) => {
    async.map(file.files, (files, callback) => {
      const imgAttr = file.body;
      const ext = path.extname(files.originalFilename);
      const imgName = shortid.generate() + ext;
      const meta = { 'ContentType' : files.headers['content-type'] };

      fs.readFile(files.path, (err, data) => {
        S3FS.writeFile(`${imgAttr.path}/${imgName}`, data, meta)
          .then(() => callback(null, { [files.fieldName] : `${Constants.S3_URL}/${imgAttr.path}/${imgName}` }),
            (error) => callback(error));
      });
    }, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  }),

  delete : (filePaths) => new Promise((resolve, reject) => {
    async.each(filePaths, (file, callback) => {
      S3FS.unlink(file.replace('https://s3-us-west-2.amazonaws.com/', ''))
        .then(() => callback(), (error) => callback(error));
    }, (err) => {
      if (err) reject(err);
      resolve(true);
    });
  }),
};

export default FilesActions;
