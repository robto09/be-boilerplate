import moment from 'moment';

const GlobalServices = {
  responseHandler : (errors, response) => ({
    error    : errors,
    response : response,
  }),
  createCustomError : (code, message, additionalInfo = {}) =>({
    code,
    content : { ...additionalInfo, error : message },
  }),
  apiUrl : (env) => {
    let url = 'http://localhost:1102';

    if (env === 'development') {
      url = 'https://dev.alltruckcr.com';
    } else if (env === 'dev-stable') {
      url = 'https://dev-stable.alltruckcr.com';
    } else if (env === 'staging') {
      url = 'https://staging.alltruckcr.com';
    } else if (env === 'production') {
      url = 'https://api.alltruckcr.com/';
    }
    return url;
  },
  generatePhoneCode : (codeLength = 4) => {
    return Math.floor(Math.random() * (Math.pow(10, (codeLength - 1)) * 9)) + Math.pow(10, (codeLength - 1));
  },
  handleGender : (gender) => new Promise((resolve, reject) => {
    try {
      if (gender === 'male') {
        resolve('M');
      } else if (gender === 'female') {
        resolve('F');
      } else {
        resolve('N');
      }
    } catch (err) {
      reject(err);
    }
  }),
  handleBirthdayCase : (facebookBirthday) => new Promise((resolve, reject) => {
    try {
      if (moment(facebookBirthday, 'MM/DD/YYYY', true).isValid()) {
        resolve(facebookBirthday);
      } else if (moment(facebookBirthday, 'YYYY', true).isValid()) {
        resolve(moment(`01-01-${facebookBirthday}`, 'MM-DD-YYYY').format('L'));
      } else if (moment(facebookBirthday, 'MM/DD', true).isValid()) {
        resolve(moment(`${facebookBirthday}/1960`, 'MM-DD-YYYY').format('L'));
      }
      resolve('01/01/1960');
    } catch (err) {
      reject(err);
    }
  }),
};

export default GlobalServices;
