import Constants from 'services/constants';
import { sendNotification } from 'services/oneSignal';
import { getLang } from 'services/helpers';

const {
  TRANSLATIONS : { PAYMENT_ERROR },
  RIDE_NOTIFICATION : { DRIVERCOPY, HEADINGS, BUTTONS, SOUND, RIDE_STATUS_MESSAGE },
} = Constants;

export default ({
  rideNotification : (playerId, params) => {

    const { language, rideId, ...anotherParams } = params;
    const rideParams = {
      ...anotherParams,
      message  : DRIVERCOPY,
      headings : HEADINGS,
      sound    : SOUND,
      // subtitle : SUBTITLE,
      buttons  : [
        { id : rideId, text : getLang(BUTTONS.accept, language) },
        { id : 0, text : getLang(BUTTONS.cancel, language) },
      ],
    };

    return sendNotification(playerId, rideParams);
  },

  paymentNotification : (playerId) => {
    const rideParams = {
      message  : DRIVERCOPY,
      headings : {
        en : 'Accept your ride',
        es : 'Acepte el ride ',
      },
    };
    return sendNotification(playerId, rideParams);
  },
  rideUpdateNotification : (playerId, status) => sendNotification(playerId, { message : RIDE_STATUS_MESSAGE(status) }), // eslint-disable-line new-cap
  paymentError           : (playerId) => sendNotification(playerId, { message : PAYMENT_ERROR }),
});
